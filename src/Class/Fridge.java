package Class;

public class Fridge extends HomeAppliance {

    private double capacity;

    public Fridge(String originStr, String type, String appliance, double capacity) {
        super(originStr, type);
        this.setAppliance(appliance);
        this.capacity = capacity;
        this.setTotalPrice(getTotal(getCapacity(), getTypePrice(), getOriginPrice()));
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    static double getTotal (double capacity, int tipePrice, int originPrice){
        double price = tipePrice + originPrice * 1.0;
        double more= (capacity - 120.0)/10;
        more=Math.round(more);
        if (capacity>120.0){
            price = price + price * 0.05 * more;
        }
        return price;
    }

}
