package Class;

import java.util.HashMap;

class HomeAppliance {

    private String originStr;
    private Integer originPrice;
    private String type;
    private Integer typePrice;
    private String appliance;
    private double totalPrice;

    public HomeAppliance(String originStr, String type){
        this.originStr=originStr;
        this.type=type;
        this.originPrice=getPriceOrigin(originStr);
        this.typePrice=getPriceType(type);
        this.appliance="Some Appliance";
        this.totalPrice=0.0;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getAppliance() {
        return appliance;
    }

    public void setAppliance(String appliance) {
        this.appliance = appliance;
    }

    public String getOriginStr() {
        return originStr;
    }

    public Integer getOriginPrice() {
        return originPrice;
    }

    public String getType() {
        return type;
    }

    public Integer getTypePrice() {
        return typePrice;
    }

    public void setOriginPrice(Integer originPrice) {
        this.originPrice = originPrice;
    }

    public void setTypePrice(Integer typePrice) {
        this.typePrice = typePrice;
    }

    public void setOriginStr(String originStr) {
        this.originStr = originStr;
    }

    public void setType(String type) { this.type = type; }

    static int getPriceOrigin(String str){

        int price;

        if(str.equals("National")){
            price=250_000;
        } else{
            price=350_000;
        }
        return price;
    }

    static int getPriceType(String typ){

        HashMap<String, Integer> consuptionMap = new HashMap<String, Integer>();

        // Add keys and values (Type, Price)
        consuptionMap.put("A", 450_000);
        consuptionMap.put("B", 350_000);
        consuptionMap.put("C", 250_000);

        //Send Price
        return consuptionMap.get(typ);
    }


}
