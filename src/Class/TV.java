package Class;

public class TV extends HomeAppliance{
    private Boolean TDT=false;
    private Boolean moreThan40=false;

    public TV(String originStr, String type, String appliance, Boolean TDT, Boolean moreThan40) {
        super(originStr, type);
        this.setAppliance(appliance);
        this.TDT=TDT;
        this.moreThan40=moreThan40;
        this.setTotalPrice(getTotal(getMoreThan40(), getTDT(), getTypePrice(), getOriginPrice() ));
    }

    public Boolean getTDT() {
        return TDT;
    }

    public void setTDT(Boolean TDT) {
        this.TDT = TDT;
    }

    public Boolean getMoreThan40() {
        return moreThan40;
    }

    public void setMoreThan40(Boolean moreThan40) {
        this.moreThan40 = moreThan40;
    }

    static Double getTotal(Boolean moreThan40, Boolean TDT, int typePrice, int originPrice){
        double price;
        price = ( typePrice + originPrice ) * 1.0;

        if( moreThan40 == true ){
            price = price * 0.3 + price;
            if( TDT == true ){
                price = price + 250_000;
            }
        }else{
            price = price;
            if( TDT == true ){
                price = price + 250_000;
            }
        }
        return price;
    }
}
