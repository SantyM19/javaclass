import Class.TV;
import Class.Fridge;

public class Main {
    public static void main(String[] args) {
        TV myObj = new TV("National","A", "TV",true, false);
        System.out.println(myObj.getOriginStr());
        System.out.println(myObj.getOriginPrice());
        System.out.println(myObj.getType());
        System.out.println(myObj.getTypePrice());

        System.out.println(myObj.getMoreThan40());
        System.out.println(myObj.getTDT());
        System.out.println(myObj.getTotalPrice());

        Fridge myObj2 = new Fridge("Imported", "B", "Fridge", 63.0);
        System.out.println(myObj2.getOriginStr());
        System.out.println(myObj2.getOriginPrice());
        System.out.println(myObj2.getType());
        System.out.println(myObj2.getTypePrice());

        System.out.println(myObj2.getCapacity());
        System.out.println(myObj2.getTotalPrice());
    }
}
